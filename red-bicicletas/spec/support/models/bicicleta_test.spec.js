var Bicicleta = require('../../../models/bicicleta')
var mongoose = require('mongoose');

describe('Testing Bicicletas', function(){
    beforeAll((done) => { mongoose.connection.close(done) });
    beforeEach(function(done) {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 5000;
        

        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
            done();
        });  
    })
    
    afterEach(function(done){
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();

        });
    });
    
    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34, -54]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34);
            expect(bici.ubicacion[1]).toEqual(-54);
        })

    });

    describe('Bicicleta.allBicis', function(){
        it('comienza vacia', function(done){
          Bicicleta.allBicis(function(err, bicis){
            expect(bicis.length).toBe(0);
            done();
          });

        });

      });


    describe('Bicicleta.add', () => {
        it('agrega solo una bici',(done)=>{
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            Bicicleta.addListener(aBici, function(err, newBicis){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bics.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici,code);
                    done();
                })
            })
        })
    })
    describe('Bicicleta.findByCode', () => {
        it('debe de devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);
                    var aBici2 = new Bicicleta({code:2, color: "roja", modelo:"urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                    if (err) console.log(err);
                    Bicicleta.findByCode(1, function(error, targetBici){
                        expect(targetBici.code).toBe(aBici.code);
                        expect(targetBici.color).toBe(aBici.color);
                        expect(targetBici.modelo).toBe(aBici.modelo);
                        done();
                    });
                    });
                });
            });  
        });  
    });
   







});















/*
beforeEach(()=> { Bicicleta.allBicis = [];});
describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);


    });


});

describe('Bicicleta.add', ()  =>{
    it('agrgamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.618059, -58.476098]  );
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });


});


describe('Bicicleta.findBy',() =>{
    it('debe devolver la bici 1', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, "verde", "urbana");
        var aBici2 = new Bicicleta(2, "naranja", "montaña");
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);

    })



})
*/
